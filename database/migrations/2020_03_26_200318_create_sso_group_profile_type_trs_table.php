<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSsoGroupProfileTypeTrsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sso_group_profile_type_trs', function (Blueprint $table) {
            $table->id();
            $table->text('name', 50);
            $table->text('description', 255);
            $table->text('lang', 5);
        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sso_group_profile_type_trs');
    }
}
